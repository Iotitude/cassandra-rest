


import iotitude.cassandra.rest.Application;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *Iotitude Cassandra REST API
 * 
 * @author Markus Malm
 * @version 1.0
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration

public class getHarmitusTest {
    
    private MockMvc mockMvc;
    
    // Set these to values found in the database
    private String testDate1 = "2017-06-13";
    private String testDate2 = "2017-06-15";
    private String endpoint = "9JQgtPwRfHxVUhp12QX/sIVYpa0=";
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        
    }
    
    @Test
    //Ignore
    public void getHarmitus() throws Exception {
        
        mockMvc.perform(get("/harmitus"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].harmitusLvl", instanceOf(Integer.class)))
                .andExpect(jsonPath("$[0].timestamp", instanceOf(Integer.class)))
                .andExpect(jsonPath("$[0].date", instanceOf(String.class)))
                .andExpect(jsonPath("$[0].endpoint", instanceOf(String.class)));
               
    }
    
    @Test
    //Ignore
    public void getHarmitusWithLimit() throws Exception {
        
        mockMvc.perform(get("/harmitus?limit=5"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[0].harmitusLvl", instanceOf(Integer.class)))
                .andExpect(jsonPath("$[0].timestamp", instanceOf(Integer.class)))
                .andExpect(jsonPath("$[0].date", instanceOf(String.class)))
                .andExpect(jsonPath("$[0].endpoint", instanceOf(String.class)));
               
    }
    
    @Test
    //@Ignore
    public void getHarmitusByDate() throws Exception {
        
        //will fail if no results are found for testDate
        
        mockMvc.perform(get("/harmitus/" + testDate1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].harmitusLvl", instanceOf(Integer.class)))
                .andExpect(jsonPath("$[0].timestamp", instanceOf(Integer.class)))
                .andExpect(jsonPath("$[0].date", instanceOf(String.class)))
                .andExpect(jsonPath("$[0].endpoint", instanceOf(String.class)));
    }
    
    @Test
    //@Ignore
    public void getHarmitusByRange() throws Exception {
        
        //will fail if no results are found for testDate
        
        mockMvc.perform(get("/harmitus/range?start=" + testDate1 + "&end=" + testDate2))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].harmitusLvl", instanceOf(Integer.class)))
                .andExpect(jsonPath("$[0].timestamp", instanceOf(Integer.class)))
                .andExpect(jsonPath("$[0].date", instanceOf(String.class)))
                .andExpect(jsonPath("$[0].endpoint", instanceOf(String.class)));
    }
    
    @Test
    //@Ignore
    public void getHarmitusByEndpointRange() throws Exception {
        
        //will fail if no results are found for testDate
        
        mockMvc.perform(get("/harmitus/endpointrange?endpoint=" + endpoint +"&start=" + testDate1 + "&end=" + testDate2))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$[0].harmitusLvl", instanceOf(Integer.class)))
                .andExpect(jsonPath("$[0].timestamp", instanceOf(Integer.class)))
                .andExpect(jsonPath("$[0].date", instanceOf(String.class)))
                .andExpect(jsonPath("$[0].endpoint", instanceOf(String.class)));
    }
}
