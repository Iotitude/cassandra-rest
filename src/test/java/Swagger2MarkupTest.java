
import com.fasterxml.jackson.databind.ObjectMapper;
import io.github.swagger2markup.Swagger2MarkupConverter;
import iotitude.cassandra.rest.Application;
import iotitude.cassandra.rest.SwaggerConfiguration;
import java.io.BufferedWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.restdocs.RestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *Iotitude Cassandra REST API
 * 
 * @author Markus Malm
 * @version 1.0
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureRestDocs(outputDir = "build/asciidoc/snippets")
@SpringBootTest(classes = {Application.class, SwaggerConfiguration.class})
@AutoConfigureMockMvc

public class Swagger2MarkupTest {

    @Rule
    public final RestDocumentation restDocumentation = new RestDocumentation("build/genrated-snippets");
    @Autowired
    private WebApplicationContext context;
    private ObjectMapper objectMapper;
    private MockMvc mockMvc;
    private RestDocumentationResultHandler document;

    @Before
    public void setUp() {
        this.document = document("{method-name}", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()));
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .alwaysDo(this.document)
                .build();
    }

    /*
    @Test
    public void GetAllHarmitus() throws Exception {
         MvcResult mvcResult = this.mockMvc.perform(get("/harmitus").accept(MediaType.APPLICATION_JSON)).andDo(document("getAllHarmitus",preprocessResponse(prettyPrint()))).andExpect(status().isOk()).andReturn();
    }
    
    @Test
    public void GetAllHarmitusByDate() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/harmitus/2017-07-04").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
    }
    
   @Test
    public void GetAllHarmitusByRange() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/harmitus/range?start=2017-07-01&end=2017-07-04").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
    }
    
    @Test
    public void GetAllHarmitusByEndpointRange() throws Exception {
        MvcResult mvcResult = this.mockMvc.perform(get("/harmitus/endpointrange?endpoint=Nu4W6wHRhTBifmy64ld74EqDWF4=&start=2017-07-01&end=2017-07-04").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
    }
    */ 
    @Test
    public void CreateSwaggerJson() throws Exception {
        String outputDir = "src/docs/asciidocs/generated";
        MvcResult mvcResult = this.mockMvc.perform(get("/v2/api-docs").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        String swaggerJson = response.getContentAsString();
        Files.createDirectories(Paths.get(outputDir));
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputDir, "swagger.json"), StandardCharsets.UTF_8)) {
            writer.write(swaggerJson);
        }

    }

    @Test
    public void convertRemoteSwaggerToAsciiDoc() {
        // Remote Swagger source
        Swagger2MarkupConverter.from(Paths.get("src/docs/asciidocs/generated/swagger.json")).build()
                .toFolder(Paths.get("src/docs/asciidoc/generated"));
    }

}
