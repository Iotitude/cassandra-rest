/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iotitude.cassandra.rest;

import com.datastax.driver.core.Cluster;
import static com.datastax.driver.core.ConsistencyLevel.QUORUM;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.NoHostAvailableException;
import java.util.ArrayList;
import java.util.UUID;
import java.util.List;
import java.util.ResourceBundle;

/**
 *Iotitude Cassandra REST API
 * 
 * @author Markus Malm
 * @version 1.0
 */
public class CassandraConnector {

    private PreparedStatement preparedQuery;
    private PreparedStatement preparedQueryByDate;
    private PreparedStatement preparedQueryByRange;
    private PreparedStatement preparedQueryByEndpointRange;
    
    private static final String query = "SELECT * FROM harmitus_logs LIMIT ?";
    private static final String queryDate = "SELECT * FROM harmitus_logs WHERE date = ? LIMIT ? ALLOW FILTERING";
    private static final String queryRange = "SELECT * FROM harmitus_logs WHERE date >= ? AND date <= ? LIMIT ? ALLOW FILTERING";
    private static final String queryEndpointRange = "SELECT * FROM harmitus_logs WHERE endpoint_keyhash = ? AND date >= ? AND date <= ? LIMIT ? ALLOW FILTERING";
    

    private Session session = null;

    public CassandraConnector() {
        ResourceBundle bundle = ResourceBundle.getBundle("cassandra");
        String[] nodes = bundle.getString("node").split(",");
        String keyspace = bundle.getString("keyspace");

        Cluster cluster = new Cluster.Builder().addContactPoints(nodes).withQueryOptions(new QueryOptions().setFetchSize(2000)).build();
        this.session = cluster.connect(keyspace);
        
        preparedQuery = session.prepare(query).setConsistencyLevel(QUORUM);
        preparedQueryByDate = session.prepare(queryDate).setConsistencyLevel(QUORUM);;
        preparedQueryByRange = session.prepare(queryRange).setConsistencyLevel(QUORUM);;
        preparedQueryByEndpointRange = session.prepare(queryEndpointRange).setConsistencyLevel(QUORUM);;

        /*ResultSet rs = session.execute("SELECT * FROM emp");
    for (Row row : rs) {
        System.out.format(row.getString("emp_last")+"\n");
    }*/
        //session.execute("INSERT INTO emp (empid, emp_dept, emp_first, emp_last) VALUES (4, 'eng', 'fired', 'dixon')");
        //cluster.close();
    }

    public void insertCassandra(String table, UUID empid, String dept, String first, String last) {
        try {
            session.execute("INSERT INTO " + table
                    + "(empid, dept, first, last) VALUES ("
                    + empid + ", " + dept + ", " + first + ", " + last + ");");
        } catch (NoHostAvailableException e) {
            System.out.println(e.getErrors());
        }
    }

    public String getEmployee(String id) {
        ResultSet rs;
        rs = session.execute("SELECT * FROM sensehat_log");

        String result = "";

        for (Row row : rs) {
            result += (row.getString(id));
            result += "<br/>";

        }

        return result;

    }

    public String getAll() {
        ResultSet rs;
        rs = session.execute("SELECT * FROM sensehat_log");
        String result = "";

        for (Row row : rs) {
            result += ("date: " + row.getString("date") + " humidity: " + row.getString("humidity") + " pressure: " + row.getString(4) + " temperature: " + row.getString(5));
            result += "<br/>";
        }
        return result;
    }

    public List getAllHarmitus(int limit) {
        List results = new ArrayList();
        ResultSet rs;
        rs = session.execute(preparedQuery.bind(limit));

        for (Row row : rs) {
            results.add(new HarmitusModel(row.getInt(3), row.getLong(4), row.getString(1), row.getString(2)));
        }
        return results;
    }

    public List getHarmitusByDate(String date, int limit) {
        List results = new ArrayList();
        ResultSet rs;
        rs = session.execute(preparedQueryByDate.bind(date, limit));

        for (Row row : rs) {
            results.add(new HarmitusModel(row.getInt(3), row.getLong(4), row.getString(1), row.getString(2)));
        }
        return results;
    }

    public List getHarmitusByRange(String start, String end, int limit) {
        List results = new ArrayList();
        ResultSet rs;
        rs = session.execute(preparedQueryByRange.bind(start, end, limit));

        for (Row row : rs) {
            results.add(new HarmitusModel(row.getInt(3), row.getLong(4), row.getString(1), row.getString(2)));
        }
        return results;
    }
    
    public List getHarmitusByEndpointRange(String endpoint, String start, String end, int limit) {
        List results = new ArrayList();
        ResultSet rs;
        rs = session.execute(preparedQueryByEndpointRange.bind(endpoint, start, end, limit));

        for (Row row : rs) {
            results.add(new HarmitusModel(row.getInt(3), row.getLong(4), row.getString(1), row.getString(2)));
        }
        return results;
    }

    public List getAllResults() {
        List results = new ArrayList();
        ResultSet rs;
        rs = session.execute("SELECT * FROM sensehat_log LIMIT 1000");

        for (Row row : rs) {
            double humidity = Double.parseDouble(row.getString("humidity"));
            double pressure = Double.parseDouble(row.getString("pressure"));
            double temperature = Double.parseDouble(row.getString("temperature"));
            results.add(new JsonModel(row.getString("date"), humidity, pressure, temperature));
        }
        return results;
    }

//Row row = rs.one();
    //return row.getString(3);
}
