/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iotitude.cassandra.rest;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *Iotitude Cassandra REST API
 * 
 * @author Markus Malm
 * @version 1.0
 */

@CrossOrigin(origins = "*")
@RestController
@Api(value = "cassandra rest", description = "Operations for querying data from cassandra")
public class DataObjectController {
    
    CassandraConnector cc = new CassandraConnector();
    
    //private static final String template = "Employee name: %s!";
    
    @RequestMapping(value ="/data/specific", method = RequestMethod.GET)
    
    public String employee(@RequestParam (value = "id", defaultValue="date")String id) {
        return cc.getEmployee(id);
        
    /*
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
        public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    */
    }
    
    @RequestMapping(value ="/data",method=RequestMethod.GET)
    public String data() {
        return cc.getAll();
    }
    
    @CrossOrigin(origins = "*")
    @RequestMapping(value ="/harmitus",method=RequestMethod.GET, produces = "application/json")
    @ApiOperation(value= "Return harmitus data", response = HarmitusModel.class)
    public List harmitusData(@RequestParam(value = "limit", defaultValue = "5000")int limit) {
        List results = cc.getAllHarmitus(limit);
        return results;
    }
    
    @RequestMapping(value ="/harmitus/{date}",method=RequestMethod.GET, produces = "application/json")
    @ApiOperation(value= "Return harmitus data from a specific date", response = HarmitusModel.class)
    public List harmitusDataByDate(
            @ApiParam(name = "date", value = "Date in YYYY-MM-DD format", required = true)
            @PathVariable String date, 
            @RequestParam(value = "limit", defaultValue = "5000")int limit) {
        List results = cc.getHarmitusByDate(date, limit);
        return results;
    }
    
    @RequestMapping(value ="/harmitus/range",method=RequestMethod.GET, produces = "application/json")
    @ApiOperation(value= "Return harmitus data from a range of dates", response = HarmitusModel.class)
    public List harmitusDataByRange(
            @ApiParam(name = "start", value = "Starting date in YYYY-MM-DD format", required = true)
            @RequestParam(value = "start") String start, 
            @ApiParam(name = "end", value = "End date in YYYY-MM-DD format", required = true)
            @RequestParam(value = "end") String end, @RequestParam (value = "limit", defaultValue = "5000") int limit) {
        List results = cc.getHarmitusByRange(start, end, limit);
        return results;
    }
    
    @RequestMapping(value ="/harmitus/endpointrange",method=RequestMethod.GET, produces = "application/json")
    @ApiOperation(value= "Return harmitus data from a range of dates from a specific endpoint", response = HarmitusModel.class)
    public List harmitusDataByEndpointRange(
            @ApiParam(name = "endpoint", value = "endpoint keyhash", required = true)
            @RequestParam(value = "endpoint") String endpoint,
            @ApiParam(name = "start", value = "Starting date in YYYY-MM-DD format", required = true)
            @RequestParam(value = "start") String start, 
            @ApiParam(name = "end", value = "End date in YYYY-MM-DD format", required = true)
            @RequestParam(value = "end") String end, @RequestParam (value = "limit", defaultValue = "5000") int limit) {
        List results = cc.getHarmitusByEndpointRange(endpoint, start, end, limit);
        return results;
    }
            
    @RequestMapping(value="/jsondata", method=RequestMethod.GET, produces = "application/json")
    @ApiOperation(value= "Return sensehat sensor data", response = JsonModel.class)
    public @ResponseBody List getJson() {
        List results = cc.getAllResults();
        return results;
        
    }
    
    
}
