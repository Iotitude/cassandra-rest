package iotitude.cassandra.rest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.google.common.base.Predicates;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;

/**
 *Iotitude Cassandra REST API
 * 
 * @author Markus Malm
 * @version 1.0
 */

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot"))     )         
          .paths(PathSelectors.any())                          
          .build()
          .apiInfo(apiInfo());
    }
    
    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(
                "Iotitude Cassandra REST API",
                "REST API for querying Iotitude Cassandra database.",
                "1.0",
                "Terms of service",
                "Markus Malm@Iotitude",
                "Apache License Version 2.0",
                "https://www.apache.org/licenses/LICENSE-2.0");
                return apiInfo;

    }
    

}
