/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iotitude.cassandra.rest;

import io.swagger.annotations.ApiModel;

/**
 *Iotitude Cassandra REST API
 * 
 * @author Markus Malm
 * @version 1.0
 */
@ApiModel
public class HarmitusModel {
    int harmitusLvl;
    long timestamp;
    String date;
    String endpoint;
    
    HarmitusModel() {
    
    }
    
    public HarmitusModel(int i, long j, String k, String l) {
        this.harmitusLvl = i;
        this.timestamp = j;
        this.date = k;
        this.endpoint = l;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getDate() {
        return date;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setHarmitusLvl(int harmitusLvl) {
        this.harmitusLvl = harmitusLvl;
    }

    public int getHarmitusLvl() {
        return harmitusLvl;
    }
    
}
