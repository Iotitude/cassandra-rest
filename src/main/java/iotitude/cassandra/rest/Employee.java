/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iotitude.cassandra.rest;

import java.util.UUID;

/**
 *Iotitude Cassandra REST API
 * 
 * @author Markus Malm
 * @version 1.0
 */
public class Employee {
    
    public UUID empid;
    public String emp_dept;
    public String emp_first;
    public String emp_last;

    
    public Employee(UUID empid, String emp_dept, String emp_first, String emp_last) {
        this.empid = empid;
        this.emp_dept = emp_dept;
        this.emp_first = emp_first;
        this.emp_last = emp_last;
    }

    public UUID getEmpid() {
        return empid;
    }

    public String getEmp_dept() {
        return emp_dept;
    }

    public String getEmp_first() {
        return emp_first;
    }

    public String getEmp_last() {
        return emp_last;
    }

    public void setEmpid(UUID empid) {
        this.empid = empid;
    }

    public void setEmp_dept(String emp_dept) {
        this.emp_dept = emp_dept;
    }

    public void setEmp_first(String emp_first) {
        this.emp_first = emp_first;
    }

    public void setEmp_last(String emp_last) {
        this.emp_last = emp_last;
    }
    

    
}
