/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iotitude.cassandra.rest;

import io.swagger.annotations.ApiModel;

/**
 *Iotitude Cassandra REST API
 * 
 * @author Markus Malm
 * @version 1.0
 */
@ApiModel
public class JsonModel {
    
    String date;
    double humidity;
    double pressure;
    double temperature;
    
    JsonModel() {
        
    }

    JsonModel(String date, double humidity, double pressure, double temperature) {
        this.date = date;
        this.humidity = humidity;
        this.pressure = pressure;
        this.temperature = temperature;
    }
    
    public void setDate(String date) {
        this.date = date;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public String getDate() {
        return date;
    }

    public double getHumidity() {
        return humidity;
    }

    public double getPressure() {
        return pressure;
    }

    public double getTemperature() {
        return temperature;
    }

}
