[source,http,options="nowrap"]
----
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Content-Length: 8631

{
  "swagger" : "2.0",
  "info" : {
    "description" : "REST API for querying Iotitude Cassandra database.",
    "version" : "1.0",
    "title" : "Iotitude Cassandra REST API",
    "termsOfService" : "Terms of service",
    "contact" : {
      "name" : "Markus Malm@Iotitude"
    },
    "license" : {
      "name" : "Apache License Version 2.0",
      "url" : "https://www.apache.org/licenses/LICENSE-2.0"
    }
  },
  "host" : "localhost:8080",
  "basePath" : "/",
  "tags" : [ {
    "name" : "data-object-controller",
    "description" : "Operations for querying data from cassandra"
  } ],
  "paths" : {
    "/data" : {
      "get" : {
        "tags" : [ "data-object-controller" ],
        "summary" : "data",
        "operationId" : "dataUsingGET",
        "consumes" : [ "application/json" ],
        "produces" : [ "*/*" ],
        "responses" : {
          "200" : {
            "description" : "OK",
            "schema" : {
              "type" : "string"
            }
          },
          "401" : {
            "description" : "Unauthorized"
          },
          "403" : {
            "description" : "Forbidden"
          },
          "404" : {
            "description" : "Not Found"
          }
        }
      }
    },
    "/data/specific" : {
      "get" : {
        "tags" : [ "data-object-controller" ],
        "summary" : "employee",
        "operationId" : "employeeUsingGET",
        "consumes" : [ "application/json" ],
        "produces" : [ "*/*" ],
        "parameters" : [ {
          "name" : "id",
          "in" : "query",
          "description" : "id",
          "required" : false,
          "type" : "string",
          "default" : "date"
        } ],
        "responses" : {
          "200" : {
            "description" : "OK",
            "schema" : {
              "type" : "string"
            }
          },
          "401" : {
            "description" : "Unauthorized"
          },
          "403" : {
            "description" : "Forbidden"
          },
          "404" : {
            "description" : "Not Found"
          }
        }
      }
    },
    "/harmitus" : {
      "get" : {
        "tags" : [ "data-object-controller" ],
        "summary" : "Return harmitus data",
        "operationId" : "harmitusDataUsingGET",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "limit",
          "in" : "query",
          "description" : "limit",
          "required" : false,
          "type" : "integer",
          "default" : 5000,
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "OK",
            "schema" : {
              "$ref" : "#/definitions/HarmitusModel"
            }
          },
          "401" : {
            "description" : "Unauthorized"
          },
          "403" : {
            "description" : "Forbidden"
          },
          "404" : {
            "description" : "Not Found"
          }
        }
      }
    },
    "/harmitus/endpointrange" : {
      "get" : {
        "tags" : [ "data-object-controller" ],
        "summary" : "Return harmitus data from a range of dates from a specific endpoint",
        "operationId" : "harmitusDataByEndpointRangeUsingGET",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "endpoint",
          "in" : "query",
          "description" : "endpoint keyhash",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "start",
          "in" : "query",
          "description" : "Starting date in YYYY-MM-DD format",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "end",
          "in" : "query",
          "description" : "End date in YYYY-MM-DD format",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "limit",
          "required" : false,
          "type" : "integer",
          "default" : 5000,
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "OK",
            "schema" : {
              "$ref" : "#/definitions/HarmitusModel"
            }
          },
          "401" : {
            "description" : "Unauthorized"
          },
          "403" : {
            "description" : "Forbidden"
          },
          "404" : {
            "description" : "Not Found"
          }
        }
      }
    },
    "/harmitus/range" : {
      "get" : {
        "tags" : [ "data-object-controller" ],
        "summary" : "Return harmitus data from a range of dates",
        "operationId" : "harmitusDataByRangeUsingGET",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "start",
          "in" : "query",
          "description" : "Starting date in YYYY-MM-DD format",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "end",
          "in" : "query",
          "description" : "End date in YYYY-MM-DD format",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "limit",
          "required" : false,
          "type" : "integer",
          "default" : 5000,
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "OK",
            "schema" : {
              "$ref" : "#/definitions/HarmitusModel"
            }
          },
          "401" : {
            "description" : "Unauthorized"
          },
          "403" : {
            "description" : "Forbidden"
          },
          "404" : {
            "description" : "Not Found"
          }
        }
      }
    },
    "/harmitus/{date}" : {
      "get" : {
        "tags" : [ "data-object-controller" ],
        "summary" : "Return harmitus data from a specific date",
        "operationId" : "harmitusDataByDateUsingGET",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "parameters" : [ {
          "name" : "date",
          "in" : "path",
          "description" : "Date in YYYY-MM-DD format",
          "required" : true,
          "type" : "string"
        }, {
          "name" : "limit",
          "in" : "query",
          "description" : "limit",
          "required" : false,
          "type" : "integer",
          "default" : 5000,
          "format" : "int32"
        } ],
        "responses" : {
          "200" : {
            "description" : "OK",
            "schema" : {
              "$ref" : "#/definitions/HarmitusModel"
            }
          },
          "401" : {
            "description" : "Unauthorized"
          },
          "403" : {
            "description" : "Forbidden"
          },
          "404" : {
            "description" : "Not Found"
          }
        }
      }
    },
    "/jsondata" : {
      "get" : {
        "tags" : [ "data-object-controller" ],
        "summary" : "Return sensehat sensor data",
        "operationId" : "getJsonUsingGET",
        "consumes" : [ "application/json" ],
        "produces" : [ "application/json" ],
        "responses" : {
          "200" : {
            "description" : "OK",
            "schema" : {
              "$ref" : "#/definitions/JsonModel"
            }
          },
          "401" : {
            "description" : "Unauthorized"
          },
          "403" : {
            "description" : "Forbidden"
          },
          "404" : {
            "description" : "Not Found"
          }
        }
      }
    }
  },
  "definitions" : {
    "HarmitusModel" : {
      "type" : "object",
      "properties" : {
        "date" : {
          "type" : "string"
        },
        "endpoint" : {
          "type" : "string"
        },
        "harmitusLvl" : {
          "type" : "integer",
          "format" : "int32"
        },
        "timestamp" : {
          "type" : "integer",
          "format" : "int64"
        }
      }
    },
    "JsonModel" : {
      "type" : "object",
      "properties" : {
        "date" : {
          "type" : "string"
        },
        "humidity" : {
          "type" : "number",
          "format" : "double"
        },
        "pressure" : {
          "type" : "number",
          "format" : "double"
        },
        "temperature" : {
          "type" : "number",
          "format" : "double"
        }
      }
    }
  }
}
----